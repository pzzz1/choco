VERIFICATION

Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

Package can be verified like this:

1. Go to

x86: https://github.com/pbek/QOwnNotes/releases/download/windows-b5324/QOwnNotes.zip

to download the ZIP-file.

2. You can use one of the following methods to obtain the checksum:
   - Use powershell function 'Get-FileHash'
   - Use Chocolatey utility 'checksum.exe'

   checksum type: sha256
   checksum32: 7D7DA707966E65329C46DDB678A32BFD96E978BA644E59C8FC80AC527C47EEA9

The included 'LICENSE.txt' file have been obtained from: https://cdn.rawgit.com/pbek/QOwnNotes/windows-b5324/LICENSE
