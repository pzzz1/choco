import-module au

$releases = 'https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/'

function global:au_GetLatest {
    $download_page = Invoke-WebRequest -Uri $releases -UseBasicParsing
    $path   = $download_page.links | ? href -match 'bit/' | select -First 1 -expand href
    $latest_release = 'https://ungoogled-software.github.io' + $path
    $latest_download_page = Invoke-WebRequest -Uri $latest_release -UseBasicParsing
    $url64   = $latest_download_page.links | ? href -match '.zip$' | select -First 1 -expand href
    $version = ($url64 -split '_|.exe' | select -First 1 -Skip 1) -replace('\.[0-9]$','')

    @{
        URL64      = $url64
        Version    = $version.Replace('-','')
        Copying    = 'https://cdn.rawgit.com/Eloston/ungoogled-chromium/' + $version + '/LICENSE'
        CopyingUrl = 'https://github.com/Eloston/ungoogled-chromium/blob/' + $version + '/LICENSE'
        ChangeLog  = 'https://github.com/Eloston/ungoogled-chromium/releases/' + $version
    }
}

function global:au_BeforeUpdate {
    Invoke-WebRequest -Uri $Latest.Copying -OutFile "legal\LICENSE.txt"
    Get-RemoteFiles -Purge -NoSuffix
}

function global:au_SearchReplace {
   @{
    ".\tools\chocolateyInstall.ps1" = @{
        "(?i)(^\s*file64\s*=\s*`"[$]toolsDir\\).*" = "`${1}$($Latest.FileName64)`""
        }
    ".\legal\VERIFICATION.txt" = @{
        "(?i)(x86_64:).*"     = "`${1} $($Latest.URL64)"
        "(?i)(checksum64:).*" = "`${1} $($Latest.Checksum64)"
        "(?i)(The included 'LICENSE.txt' file have been obtained from:).*"  = "`${1} $($Latest.Copying)"
        }
    "ungoogled-chromium.nuspec" = @{
          "(\<licenseUrl\>).*(\<\/licenseUrl\>)"     = "`${1}$($Latest.CopyingUrl)`$2"
          "(\<releaseNotes\>).*(\<\/releaseNotes\>)" = "`${1}$($Latest.ChangeLog)`$2"
        }
    }
}

update  -ChecksumFor none
