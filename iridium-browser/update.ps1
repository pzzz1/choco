import-module au

$releases = 'https://downloads.iridiumbrowser.de/windows/'

function global:au_GetLatest {
    $downloads = Invoke-WebRequest -Uri $releases -UseBasicParsing
    $version   = ($downloads.links | ? href -match '/$' |  select -Last 1 -expand href) -replace '/',''
    $folder    = 'https://downloads.iridiumbrowser.de/windows/' + $version + '/'
    $current = Invoke-WebRequest -Uri $folder -UseBasicParsing
    $url32 = $current.links | ? href -match '-x86.msi$' |  select -First 1 -expand href
    $url64 = $current.links | ? href -match '-x64.msi$' |  select -First 1 -expand href

    $log = Invoke-WebRequest -Uri 'https://git.iridiumbrowser.de/cgit.cgi/iridium-browser/log/?h=patchview' -UseBasicParsing
    $changelog = $log.links | ? { $_.href -match '&amp;id=' } | select -First 1 -expand href
    $tag = $version -replace '-.*$', ''

    @{
        URL32      = $folder + $url32
        URL64      = $folder + $url64
        Version    = ($version).Replace('-','.')
        ChangeLog  = 'https://git.iridiumbrowser.de/' + $changelog
        CopyingUrl = 'https://git.iridiumbrowser.de/cgit.cgi/iridium-browser/tree/LICENSE'
    }
}

function global:au_SearchReplace {
   @{
    ".\tools\chocolateyInstall.ps1" = @{
        "(?i)(^\s*url\s*=\s*)('.*')"        = "`$1'$($Latest.URL32)'" 
        "(?i)(^\s*checksum\s*=\s*)('.*')"   = "`$1'$($Latest.Checksum32)'"
        "(?i)(^\s*url64bit\s*=\s*)('.*')"      = "`$1'$($Latest.URL64)'"
        "(?i)(^\s*checksum64\s*=\s*)('.*')" = "`$1'$($Latest.Checksum64)'"
    }
    "iridium-browser.nuspec" = @{
        "(\<licenseUrl\>).*(\<\/licenseUrl\>)"     = "`${1}$($Latest.CopyingUrl)`$2"
        "(\<releaseNotes\>).*(\<\/releaseNotes\>)" = "`${1}$($Latest.ChangeLog)`$2"
    }
  }
}

update
