import-module au

$news = 'http://pmetro.chpeks.com/News.html'

function global:au_GetLatest {
    $page = Invoke-WebRequest -Uri $news -UseBasicParsing
    $date = $page.content -split ' '| Select-String -Pattern "<p>" | select -First 1
    $version = ($date -split 'p>' -replace '</','' | %{ $_.Split('.')[2,1,0],3; }) -replace ' ','.' | select -First 1 -Skip 2

    @{
        URL32   = 'http://pmetro.chpeks.com/download/pMetroSetup.exe'
        Version = $version
    }
}

function global:au_SearchReplace {
    @{
        "tools\chocolateyInstall.ps1" = @{
            "(?i)(^\s*url\s*=\s*)('.*')"      = "`$1'$($Latest.URL32)'" 
            "(?i)(^\s*checksum\s*=\s*)('.*')" = "`$1'$($Latest.Checksum32)'"
        }
    }
}

update
