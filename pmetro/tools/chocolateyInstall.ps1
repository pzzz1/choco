﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'http://pmetro.chpeks.com/download/pMetroSetup.exe'
  checksum       = '05ff7a5c80e51f0acf23aa5b6d5f09a6590112ff7b88bfbedf32365b717d1d21'
  checksumType   = 'sha256'
  silentArgs     = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-'
}

Install-ChocolateyPackage @packageArgs

Remove-Item $toolsDir\*.exe -ea 0
